class FakeResponse:
    def __init__(self, status_code, mm_error=""):
        self.status_code = status_code
        self.mm_error = mm_error

    def json(self):
        if self.mm_error:
            return self.mm_error
