import unittest2 as unittest
from pymasmovil.models.order_item import OrderItem
from pymasmovil.models.session import Session
from pymasmovil.models.account import Account
from pymasmovil.errors.exceptions import NewLineRequestRequiredParamsError
from mock import patch, Mock


class OrderItemTests(unittest.TestCase):
    def setUp(self):
        self.session = Session("sample-session-id")
        self.expected_id = "some-order-item-id"
        self.expected_route = OrderItem._route
        self.sample_order_item_post = {
            "lineInfo": [
                {
                    "idLine": 1,
                    "name": "sample-name",
                    "surname": "sample-surname",
                    "phoneNumber": "sample-phone",
                    "documentType": "sample-doc-type",
                    "docid": "sample-doc-number",
                    "iccid": "sample-iccid",
                    "iccid_donante": "sample-donor-iccid",
                    "donorOperator": "sample-donor",
                    "productInfo": {"id": "0000001", "additionalBonds": []},
                }
            ]
        }
        self.account = Account(id="some-account-id")

    @patch("pymasmovil.models.contract.Client")
    def test_get(self, MockClient):

        expected_get_route = "{}/{}".format(self.expected_route, self.expected_id)
        expected_response = {"orderDetail": {"id": self.expected_id}}

        def client_get_side_effect(route):
            if route == expected_get_route:
                return expected_response

        MockClient.return_value = Mock(spec=["get"])
        MockClient.return_value.get.side_effect = client_get_side_effect

        order_item = OrderItem.get(self.session, self.expected_id)

        self.assertEqual(order_item.id, self.expected_id)

    @patch("pymasmovil.models.contract.Client")
    def test_get_with_portability_date(self, MockClient):

        expected_get_route = "{}/{}".format(self.expected_route, self.expected_id)
        expected_date = "2021-07-13"
        expected_response = {
            "orderDetail": {
                "id": self.expected_id,
                "tarAttributes": {
                    "Fecha_planificada_de_entrega": expected_date
                }
            }
        }

        def client_get_side_effect(route):
            if route == expected_get_route:
                return expected_response

        MockClient.return_value = Mock(spec=["get"])
        MockClient.return_value.get.side_effect = client_get_side_effect

        order_item = OrderItem.get(self.session, self.expected_id)

        self.assertEqual(order_item.tarAttributes.get("Fecha_planificada_de_entrega"),
                         expected_date)

    @patch("pymasmovil.models.order_item.Client")
    def test_post(self, MockClient):

        expected_post_route = "{}/accounts/{}/order-items".format(
            self.expected_route, self.account.id
        )
        expected_response = {}

        def client_post_side_effect(route, params, body):
            if route == expected_post_route and body == self.sample_order_item_post:
                return expected_response

        MockClient.return_value = Mock(spec=["post"])
        MockClient.return_value.post.side_effect = client_post_side_effect

        order_item = OrderItem.create(
            self.session, self.account, **self.sample_order_item_post
        )

        self.assertEqual(
            order_item.phone, self.sample_order_item_post["lineInfo"][0]["phoneNumber"]
        )

    @patch("pymasmovil.models.order_item.Client")
    def test_post_incomplete_new_number_registration(self, MockClient):

        new_registration_post = {
            "lineInfo": [
                {
                    "idLine": 1,
                    "documentType": "CIF",
                    "productInfo": {"id": "0000001", "additionalBonds": []},
                }
            ]
        }
        self.assertRaisesRegex(
            NewLineRequestRequiredParamsError,
            "Missing or empty attributes required for the requested "
            + "new phone number registration: corporateName, iccid",
            OrderItem.create,
            self.session,
            self.account,
            **new_registration_post
        )

    @patch("pymasmovil.models.order_item.Client")
    def test_post_incomplete_portability(self, MockClient):

        self.sample_order_item_post["lineInfo"][0].pop("donorOperator")

        self.assertRaisesRegex(
            NewLineRequestRequiredParamsError,
            "Missing or empty attributes required for the requested "
            + "portability: donorOperator",
            OrderItem.create,
            self.session,
            self.account,
            **self.sample_order_item_post
        )
