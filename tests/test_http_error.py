import unittest2 as unittest

from pymasmovil.errors.http_error import HTTPError


class FakeMMError:
    def __init__(self, status_code, message, fields):
        self.status_code = status_code
        self.message = message
        self.fields = fields


class HTTPErrorTests(unittest.TestCase):
    def test_constructor(self):
        status_code = 400
        message = "Debe informar el nombre del cliente"
        fields = "first_name"
        fake_mm_error = FakeMMError(status_code, message, fields)

        http_error = HTTPError(fake_mm_error)

        self.assertEqual(http_error.message, message)
        self.assertEqual(http_error.fields, fields)
        self.assertEqual(http_error.status_code, status_code)
