import unittest2 as unittest
from pymasmovil.models.order_item import OrderItem
from pymasmovil.models.asset import Asset
from pymasmovil.models.account import Account
from pymasmovil.models.session import Session
from mock import patch, Mock

from pymasmovil.errors.exceptions import (AccountRequiredParamsError,
                                          OrderItemNotFoundByICC)


class AccountTests(unittest.TestCase):
    def setUp(self):
        self.expected_id = "some-account-id"
        self.session = Session("sample-session-id")
        self.expected_v1_route = Account._v1_route
        self.expected_v2_route = Account._v2_route
        self.new_account = {
            "name": "test-name",
            "surname": "surname-test",
            "documentNumber": "fake-id",
            "email": "fake3@example.com",
            "phone": "666",
            "postalCode": "08015",
            "province": "08",
            "region": 9,
            "roadNumber": 15,
            "roadType": "Carrer",
            "roadName": "Llarg",
            "town": "Barcelona",
        }
        self.account = Account(id="000001")
        self.order_item_list_response = {
            "rows": [
                {
                    "id": "0000002",
                    "simAttributes": {"ICCID": "727272727"}
                },
                {
                    "id": "0000003",
                    "simAttributes": {"ICCID": "333"}
                },
                {
                    "id": "0000004",
                    "simAttributes": {"ICCID": "888888"}
                },
            ]
        }

    @patch("pymasmovil.models.account.Client")
    def test_get(self, MockClient):

        expected_get_route = "{}/{}".format(self.expected_v1_route, self.expected_id)
        expected_response = {"id": self.expected_id}

        def client_get_side_effect(route):
            if route == expected_get_route:
                return expected_response

        MockClient.return_value = Mock(spec=["get"])
        MockClient.return_value.get.side_effect = client_get_side_effect

        account = Account.get(self.session, self.expected_id)

        self.assertEqual(account.id, self.expected_id)

    @patch("pymasmovil.models.account.Client")
    def test_get_by_NIF(self, MockClient):

        expected_get_route = self.expected_v2_route
        expected_NIF = "12345678A"
        expected_params = {
            "rowsPerPage": 1,
            "actualPage": 1,
            "documentNumber": expected_NIF,
        }
        expected_account_got_by_NIF = {
            "rows": [
                {
                    "id": self.expected_id,
                }
            ]
        }

        def client_get_side_effect(route, **params):
            if route == expected_get_route and params == expected_params:
                return expected_account_got_by_NIF

        MockClient.return_value = Mock(spec=["get"])
        MockClient.return_value.get.side_effect = client_get_side_effect

        account = Account.get_by_NIF(self.session, expected_NIF)

        self.assertEqual(account.id, self.expected_id)

    @patch("pymasmovil.models.account.Client")
    def test_create_particular_account(self, MockClient):

        self.new_account.update(
            {
                "documentType": "2",
            }
        )

        def client_post_side_effect(route, _params, body):
            if route == self.expected_v2_route and body == self.new_account:
                return self.expected_id

        MockClient.return_value = Mock(spec=["post"])
        MockClient.return_value.post.side_effect = client_post_side_effect

        account = Account.create(self.session, **self.new_account)

        self.assertEqual(account.name, self.new_account["name"])

    @patch("pymasmovil.models.account.Client")
    def test_create_particular_account_missing_name(self, MockClient):

        self.new_account.update({"documentType": "2", "name": None})

        def client_post_side_effect(route, _params, body):
            if route == self.expected_v2_route and body == self.new_account:
                return self.expected_id

        MockClient.return_value = Mock(spec=["post"])
        MockClient.return_value.post.side_effect = client_post_side_effect

        self.assertRaisesRegex(
            AccountRequiredParamsError,
            "Missing or empty attributes required to create an account:" + " name",
            Account.create,
            self.session,
            **self.new_account
        )

    @patch("pymasmovil.models.account.Client")
    def test_create_company_account(self, MockClient):

        self.new_account.update(
            {
                "documentType": "1",
                "corporateName": "SC",
            }
        )

        def client_post_side_effect(route, _params, body):
            if route == self.expected_v2_route and body == self.new_account:
                return self.expected_id

        MockClient.return_value = Mock(spec=["post"])
        MockClient.return_value.post.side_effect = client_post_side_effect

        account = Account.create(self.session, **self.new_account)

        self.assertEqual(account.corporateName, self.new_account["corporateName"])

    @patch("pymasmovil.models.account.Client")
    def test_create_company_account_missing_corporate_name(self, MockClient):

        self.new_account.update({"documentType": "1"})

        def client_post_side_effect(route, _params, body):
            if route == self.expected_v2_route and body == self.new_accoun:
                return self.expected_id

        MockClient.return_value = Mock(spec=["post"])
        MockClient.return_value.post.side_effect = client_post_side_effect

        self.assertRaisesRegex(
            AccountRequiredParamsError,
            "Missing or empty attributes required to create an account:"
            + " corporateName",
            Account.create,
            self.session,
            **self.new_account
        )

    @patch("pymasmovil.models.account.Client")
    def test_get_order_item_or_asset_by_phone(self, MockClient):

        expected_get_route = "{}/{}/".format(self.expected_v1_route, self.account.id)
        expected_params = {"rowsPerPage": 1, "actualPage": 1, "phone": 666666666}
        expected_response = {"rows": [{"id": "0000002"}]}

        def client_get_side_effect(route, **params):
            if (
                route == expected_get_route + "order-items"
                or route == expected_get_route + "assets"
                and params == expected_params
            ):
                return expected_response

        MockClient.return_value = Mock(spec=["get"])
        MockClient.return_value.get.side_effect = client_get_side_effect

        order_item = self.account.get_order_item_by_phone(self.session, 666666666)

        self.assertTrue(isinstance(order_item, OrderItem))
        self.assertEqual(order_item.id, "0000002")

        asset = self.account.get_asset_by_phone(self.session, 666666666)

        self.assertTrue(isinstance(asset, Asset))
        self.assertEqual(asset.id, "0000002")

    @patch("pymasmovil.models.account.Client")
    def test_get_order_item_by_ICC_OK(self, MockClient):
        expected_get_route = "{}/{}/order-items".format(
            self.expected_v1_route, self.account.id)
        expected_params = {"rowsPerPage": 30, "actualPage": 1}

        def client_get_side_effect(route, **params):
            if (
                route == expected_get_route
                and params == expected_params
            ):
                return self.order_item_list_response

        MockClient.return_value = Mock(spec=["get"])
        MockClient.return_value.get.side_effect = client_get_side_effect

        order_item = self.account.get_order_item_by_ICC(self.session, 333)

        self.assertTrue(isinstance(order_item, OrderItem))
        self.assertEqual(order_item.id, "0000003")

    @patch("pymasmovil.models.account.Client")
    def test_get_newest_order_item_by_ICC_when_more_than_one(self, MockClient):
        order_item_list_icc_repeated = {
            "rows": [
                {
                    "id": "0000001",
                    "createdDate": "2021-08-30T14:28:12.000Z",
                    "simAttributes": {"ICCID": "727272727"}
                },
                {
                    "id": "0000002",
                    "createdDate": "2021-08-30T14:28:00.000Z",
                    "simAttributes": {"ICCID": "727272727"}
                },
                {
                    "id": "0000003",
                    "createdDate": "2022-08-30T14:28:00.000Z",  # newest
                    "simAttributes": {"ICCID": "727272727"}
                },
                {
                    "id": "0000004",
                    "createdDate": "2021-09-10T14:28:00.000Z",
                    "simAttributes": {"ICCID": "727272727"}
                },
                {
                    "id": "0000005",
                    "createdDate": "2021-09-10T14:28:00.000Z",
                    "simAttributes": None
                    # There can be broken order-items (missing 'simAttributes' parameter)
                    # within the MM Community system
                },
            ]
        }
        MockClient.return_value = Mock(spec=["get"])
        MockClient.return_value.get.return_value = order_item_list_icc_repeated

        order_item = self.account.get_order_item_by_ICC(
            self.session, 727272727)

        self.assertTrue(isinstance(order_item, OrderItem))
        self.assertEqual(order_item.id, "0000003")

    @patch("pymasmovil.models.account.Client")
    def test_get_non_KO_order_item_by_ICC(self, MockClient):
        order_item_list_icc_repeated = {
            "rows": [
                {
                    "id": "0000001",
                    "status": "Esperando para cancelar",
                    "simAttributes": {"ICCID": "787867656"}
                },
                {
                    "id": "0000002",
                    "status": "Cancelada",
                    "simAttributes": {"ICCID": "727272727"}
                },
                {
                    "id": "0000003",
                    "status": "Error",
                    "simAttributes": {"ICCID": "727272727"}
                },
                {
                    "id": "0000004",
                    "status": "Pendiente",  # Non KO
                    "simAttributes": {"ICCID": "727272727"}
                },
            ]
        }
        MockClient.return_value = Mock(spec=["get"])
        MockClient.return_value.get.return_value = order_item_list_icc_repeated

        order_item = self.account.get_order_item_by_ICC(
            self.session, 727272727, non_KO=True)

        self.assertTrue(isinstance(order_item, OrderItem))
        self.assertEqual(order_item.id, "0000004")

    @patch("pymasmovil.models.account.Client")
    def test_get_order_item_by_ICC_KO(self, MockClient):
        expected_get_route = "{}/{}/order-items".format(
            self.expected_v1_route, self.account.id)
        expected_params = {"rowsPerPage": 30, "actualPage": 1}

        def client_get_side_effect(route, **params):
            if (
                route == expected_get_route
                and params == expected_params
            ):
                return self.order_item_list_response

        MockClient.return_value = Mock(spec=["get"])
        MockClient.return_value.get.side_effect = client_get_side_effect

        ICC = 121212
        self.assertRaisesRegex(
            OrderItemNotFoundByICC,
            "No order item with ICC: {} can be found in the account with id: {}".format(  # noqa
                ICC, self.account.id
            ),
            self.account.get_order_item_by_ICC,
            self.session,
            ICC
        )
