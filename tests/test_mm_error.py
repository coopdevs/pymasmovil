import unittest2 as unittest

from pymasmovil.errors.mm_error import MMError
from pymasmovil.errors.exceptions import UnknownMMError
from tests.fake_response import FakeResponse


class MMErrorTests(unittest.TestCase):
    def test_build_estandard_MM_error(self):

        missing_tariff_id_error = {
            "success": False,
            "errors": [
                {
                    "statusCode": "400",
                    "message": "El campo id debe estar relleno.",
                    "fields": "id",
                }
            ],
        }
        response_missing_tariff_id = FakeResponse(400, missing_tariff_id_error)

        mm_error_missing_tariff_id = MMError()
        mm_error_missing_tariff_id.build(response_missing_tariff_id)

        self.assertEqual(
            mm_error_missing_tariff_id.message, "El campo id debe estar relleno."
        )
        self.assertEqual(mm_error_missing_tariff_id.status_code, "400")
        self.assertEqual(mm_error_missing_tariff_id.fields, "id")

    def test_build_MM_error_extra_array(self):

        duplicated_account_error = [
            {
                "success": False,
                "errors": [
                    {
                        "statusCode": "500",
                        "message": "Insert failed. First exception on row 0; first error: \
                            DUPLICATES_DETECTED, El cliente residencial que está intentando \
                            crear/modificar ya existe asociado al cableoperador, por favor, \
                            busque entre los existentes por el número de documento: []",
                        "fields": "",
                    }
                ],
            }
        ]
        response_duplicated_account = FakeResponse(400, duplicated_account_error)

        mm_error_duplicated_account = MMError()
        mm_error_duplicated_account.build(response_duplicated_account)

        self.assertTrue("DUPLICATES_DETECTED" in mm_error_duplicated_account.message)
        self.assertEqual(mm_error_duplicated_account.status_code, "500")
        self.assertEqual(mm_error_duplicated_account.fields, "")

    def test_build_invalid_session_MM_error(self):

        session_expired_error = [
            {"message": "Session expired or invalid", "errorCode": "INVALID_SESSION_ID"}
        ]
        response_session_expired = FakeResponse(500, session_expired_error)

        mm_error_session_expired = MMError()
        mm_error_session_expired.build(response_session_expired)

        self.assertEqual(
            mm_error_session_expired.message,
            "INVALID_SESSION_ID: Session expired or invalid",
        )
        self.assertEqual(mm_error_session_expired.status_code, 500)

    def test_build_other_error(self):

        other_error = {
            "error_code": "400",
            "body": [{"message": "another error in another structure"}],
        }
        response_other_error = FakeResponse(400, other_error)

        mm_error_session_expired = MMError()

        self.assertRaises(
            UnknownMMError, mm_error_session_expired.build, response_other_error
        )
